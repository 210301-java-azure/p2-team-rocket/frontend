import { EventEmitter, Injectable, Output } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.prod';
import { JsonPipe } from '@angular/common';
import { User } from './user-profile/user';


@Injectable({
  providedIn: 'root'
})
export class RegisterServiceService {

  registerUrl: string=environment.baseUrl+"register";
  loginUrl: string =environment.baseUrl+"user/login";
  @Output() user: EventEmitter<User> = new EventEmitter();

  constructor(private http:HttpClient) { }

  attemptRegister(username: string, password: string, email: string):Observable<any>{

    const head = {'content-type':'application/json','Authorization':'auth'};
    const payload = `{
      "username": "${username}",
      "password": "${password}",
      "email": "${email}"
    }`;

    return this.http.post(this.registerUrl, payload, {'headers':head});
  }

  attemptLogin(username: string, password: string):Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    const payload = `{
      "username": "${username}",
      "password": "${password}"
    }`;
    return this.http.post(this.loginUrl, payload,{'headers':head});
  }

}
