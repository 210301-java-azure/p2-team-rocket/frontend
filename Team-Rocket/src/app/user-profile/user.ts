
export interface User{
    id: number;
    username: string;
    email: string;
    avatar: number;
}