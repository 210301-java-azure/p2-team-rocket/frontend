
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {avatarPics, banner} from 'src/assets/pics';
import { RegisterServiceService } from '../register-service.service';
import { UserService } from '../user.service';
import { User } from './user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {

  bannerImg: string = "../assets/user-profile-banner.jpg";
  userPicJessie = avatarPics[1];
  userPicJames = avatarPics[0];
  user: User;
  avatar;

  username:string;
 
  constructor(private us: UserService, private reg: RegisterServiceService, private router:Router) {
    this.user = {id:0,username:"",email:"",avatar:0};
    this.username = sessionStorage.getItem("username") || "";

    if(this.username==""){
      this.router.navigate(["home"]);
    }else{      
        this.us.getUser(this.username).subscribe((res)=>{
          this.user = res;  
          console.log(this.user);
        }); 
        this.avatar= avatarPics[this.user.id];
    }
  

   }

  ngOnInit(): void {     
    
  }

  newAvatar(index:number){
    this.avatar = avatarPics[index];
    this.user.avatar = index; 
    console.log(this.user.avatar);
  }

  onSave(){   

    
    //let temp: User = {id:this.user.id,username:this.user.username,email:this.user.email,avatar:this.avatarId};    
    this.us.updateUser(this.user.id,this.user.username,this.user.email,this.user.avatar).subscribe((res)=>{  
      this.reg.user.emit(res);   
      sessionStorage.setItem("username",this.user.username);   
    });  
    console.log(this.user);
  }

}
