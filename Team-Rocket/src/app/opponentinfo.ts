import { PokemonInfo } from "./pokemoninfo";

export interface OpponentInfo{
    id: number;
    name?: string;
    pokemon: PokemonInfo[];
}