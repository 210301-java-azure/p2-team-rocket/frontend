import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterServiceService } from '../register-service.service';
import { User } from '../user-profile/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
username:string="";
password:string="";


constructor(private reg:RegisterServiceService, private router:Router) { }

  ngOnInit(): void {
  }
  login(){        
    this.reg.attemptLogin(this.username, this.password).subscribe((res)=>{
      let temp: User = {id:res.userId,username:res.username,email:res.email,avatar:res.avatarId};   
      this.reg.user.emit(res); 
      this.router.navigate(["home"]);
    }) 
  }
}
