import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment} from 'src/environments/environment';
import { Pokemon } from './pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  pokeUrl: string =environment.baseUrl+"pokemon";
  oppUrl: string = environment.baseUrl+"opponent"
  pokeApiInfo:string = "https://pokeapi.co/api/v2/pokemon/"
  pokeApiMove:string = "https://pokeapi.co/api/v2/move/"
  constructor(private http:HttpClient) { }

  getAllPokemon():Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    return this.http.get(this.pokeUrl,{'headers':head})
  }

  getPokeInfo(pokeid:number):Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    return this.http.get(this.pokeApiInfo+pokeid,{'headers':head})

  }
  getMoveInfo(move:number):Observable<any>{
    const head = {'content-type':'application/json'};
    return this.http.get(this.pokeApiMove+move,{'headers':head})
  }
  calculateHP(hp:number):number{
    return Math.floor(.01*(2*hp+15+(0)*5)+5+10)
  }
  calculateStat(stat:number):number{
    return Math.floor(0.01 * (2 * stat + 15 + 0) * 5 + 5);
  }


  getAllOpponents():Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    return this.http.get(this.oppUrl,{'headers':head})
  }
}
