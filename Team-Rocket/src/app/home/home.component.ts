import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  imageUrl: string ="https://static0.cbrimages.com/wordpress/wp-content/uploads/2021/01/team-rocket.jpg";
  constructor() { }

  ngOnInit(): void {
  }

}
