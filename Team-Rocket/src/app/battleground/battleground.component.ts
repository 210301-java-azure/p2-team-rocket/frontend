import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BattleService } from '../battle.service';
import { Move } from '../move';
import { PokemonInfo } from "../pokemoninfo";



@Component({
  selector: 'app-battleground',
  templateUrl: './battleground.component.html',
  styleUrls: ['./battleground.component.css']
})
export class BattlegroundComponent implements OnInit {

  @Input() playerPokemon: PokemonInfo={id:0};
  @Input() opponentPokemon: PokemonInfo={id:0};

  firstmove:boolean=true;
  turn:boolean=true;
  battlePrompt:string="";

  playerHealth: number=0;
  opponentHealth: number=0;
  oHealth:number=100;
  pHealth:number=100;
  winMessage: String="Welcome to Team Rocket!"
  loseMessage: String="Better luck next time!"

  runButton: String="../assets/run.png"
  fightButton: String="../assets/fight.png"
  @ViewChild('run') run:any;

  constructor(private battleservice: BattleService,private route:Router) {
  }

  ngOnInit(): void {
  }

  opponentTurn(){
    let count:number=0;
    if(this.opponentPokemon?.move1?.id!=0){
        count++;
    }
    if(this.opponentPokemon?.move2?.id!=0){
      count++;
    }
    if(this.opponentPokemon?.move3?.id!=0){
      count++;
    }
    if(this.opponentPokemon?.move4?.id!=0){
      count++;
    }
    let move:number=Math.floor((Math.random()*count)+1);
    switch(move){
      case 1:
        this.opponentAttack(this.opponentPokemon?.move1?.power!);
        this.battlePrompt=this.opponentPokemon.name!+" used: "+this.opponentPokemon.move1?.name!;
        break;

      case 2:
        this.opponentAttack(this.opponentPokemon?.move2?.power!);
        this.battlePrompt=this.opponentPokemon.name!+" used: "+this.opponentPokemon.move2?.name!;
        break;

      case 3:
        this.opponentAttack(this.opponentPokemon?.move3?.power!);
        this.battlePrompt=this.opponentPokemon.name!+" used: "+this.opponentPokemon.move3?.name!;
        break;

      case 4:
        this.opponentAttack(this.opponentPokemon?.move4?.power!);
        this.battlePrompt=this.opponentPokemon.name!+" used: "+this.opponentPokemon.move4?.name!;
        break;
    }
  }

  playerAttack(move:Move) {
    if(this.firstmove){
      this.playerHealth=this.playerPokemon.hp!;
      this.opponentHealth=this.opponentPokemon.hp!;
      this.firstmove=false;
    }
    let damage:number=this.battleservice.damageCalculator(move.power!,this.playerPokemon.atk!,this.opponentPokemon.def!)
    this.battlePrompt=this.playerPokemon.name!+" used: "+move.name!;
    this.opponentHealth= this.opponentHealth-damage;
    if(this.opponentHealth<=0){
      this.opponentHealth=0;
      this.oHealth=(this.opponentHealth/this.opponentPokemon.hp!)*100;
    }else if(this.opponentHealth>0){
      this.oHealth=(this.opponentHealth/this.opponentPokemon.hp!)*100;
      this.turn = false;
      setTimeout((res:any)=>{
      this.opponentTurn();
      },2000);
    }
  }
  opponentAttack(pwr:number){
    let damage:number=this.battleservice.damageCalculator(pwr,this.opponentPokemon.atk!,this.playerPokemon.def!)
    this.playerHealth= this.playerHealth-damage;
    if(this.playerHealth<0){
      this.playerHealth=0;
      this.pHealth=(this.playerHealth/this.playerPokemon.hp!*100);
    }
    this.pHealth=(this.playerHealth/this.playerPokemon.hp!*100);
    this.turn = true;
  }
  endMatch(): void{
    this.route.navigate(["home"]);
  }
  playAudio(){
    let audio = new Audio();
    audio.src ="";
    audio.load();
    audio.play();
  }
}
