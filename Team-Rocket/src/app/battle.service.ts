import { Injectable } from '@angular/core';
import { PokemonInfo } from './pokemoninfo';

@Injectable({
  providedIn: 'root'
})
export class BattleService {

  constructor() { }

  damageCalculator(power: number, atk: number, def: number): number{
    return Math.floor(((4*power*(atk/def))/50)+2)
  }

}
