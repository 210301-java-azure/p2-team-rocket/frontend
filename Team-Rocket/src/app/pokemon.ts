export interface Pokemon{
    id: number;
    api_id: number;
    move_1: number;
    move_2: number;
    move_3: number;
    move_4: number;
}