import { Pokemon } from "./pokemon";

export interface Opponent{
    id: number;
    name?: string;
    pokemonSet: Pokemon[];
}