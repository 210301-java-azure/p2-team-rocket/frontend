import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { environment } from '../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userProfileUrl: string =environment.baseUrl+"user/update";

  constructor(private http:HttpClient) { }

  updateUser(userId: any, username: any, useremail: string, avatarId: number):Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    const payload = `{
      "id":${userId},
      "username": "${username}",
      "avatar": "${avatarId}",
      "email": "${useremail}"
    }`;
    return this.http.patch(this.userProfileUrl, payload, {'headers':head});
  }
  getUser(username: string):Observable<any>{
    const head = {'content-type':'application/json','Authorization':'auth'};
    return this.http.get(environment.baseUrl+"user/"+`${username}`,{'headers':head})
  }

}
