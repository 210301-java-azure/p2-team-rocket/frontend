import { Move } from "./move";

export interface PokemonInfo{
    id: number;
    name?: string;
    api_id?: number;
    atk?: number;
    def?: number;
    hp?: number;
    move1?: Move;
    move2?: Move;
    move3?: Move;
    move4?: Move;
    sprite?: string;
    opp_sprite?: string;
}