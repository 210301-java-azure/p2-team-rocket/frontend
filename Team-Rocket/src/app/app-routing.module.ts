import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BattleStationComponent } from './battle-station/battle-station.component';
import {HomeComponent} from './home/home.component' ;
import {RegisterComponent} from './register/register.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import { BattlegroundComponent } from './battleground/battleground.component';

const routes: Routes = [
{path:"home", component:HomeComponent},
{path:"register", component:RegisterComponent},
{path:"user-profile", component:UserProfileComponent},
{path:"battle-station", component:BattleStationComponent},
{path: "battleground", component:BattlegroundComponent},
{path:"", redirectTo: "home", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
