import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleStationComponent } from './battle-station.component';

describe('BattleStationComponent', () => {
  let component: BattleStationComponent;
  let fixture: ComponentFixture<BattleStationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BattleStationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
