import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import {avatarPics ,teamrocketSprites} from '../../assets/pics'
import { Pokemon } from '../pokemon';
import { Move } from '../move';
import { PokemonService } from '../pokemon.service';
import { User } from '../user-profile/user';
import { PokemonInfo } from '../pokemoninfo';
import { Opponent } from '../opponent';
import { OpponentInfo } from '../opponentinfo';
import { RegisterServiceService } from '../register-service.service';

@Component({
  selector: 'app-battle-station',
  templateUrl: './battle-station.component.html',
  styleUrls: ['./battle-station.component.css']
})
export class BattleStationComponent implements OnInit {

  bannerImg: string = "../assets/battle-station.png";
  pics = avatarPics;
  trSprites = teamrocketSprites;
  num:number =0;
  userAvatar;
  oppSprite;
  temp: string="";
  pokeChoice:PokemonInfo ={id:0};
  oppChoice:PokemonInfo ={id:0};  
  username:string;
  pokes:Pokemon[];
  pokesInfo:PokemonInfo[];
  opps:Opponent[];
  oppsInfo:OpponentInfo[];




  constructor(private router:Router, private poke:PokemonService,private reg:RegisterServiceService) 
  {  
    this.oppSprite=this.trSprites[3];
    this.pokes=new Array();    
    this.pokesInfo=new Array();
    this.opps=new Array();
    this.oppsInfo=new Array();
    this.reg.user.subscribe((res)=>{
      this.num=res.avatar

    })

    this.username = sessionStorage.getItem("username") || "";
    if(this.username==""){
      this.router.navigate(["home"]);
    }else{
      this.temp = sessionStorage.getItem("avatarId")||"";
      this.num = +this.temp;
      this.userAvatar = this.pics[this.num];
    }
    this.getPokemon();
    this.getOppoents();
  }

  ngOnInit(): void {
  }

  getPokemon(){
    this.poke.getAllPokemon().subscribe((res)=>{
      this.pokes = res;      
     this.pokesInfo=this.convertPokemon(this.pokes);
  
    })
  }
  getOppoents(){
    this.poke.getAllOpponents().subscribe((res)=>{
      this.opps = res;
      this.oppsInfo= this.convertOpponents(this.opps);
    })
  }
  convertOpponents(opps:Opponent[]):OpponentInfo[]{
    let i:number;
    let oppsInfo:OpponentInfo[] = new Array();
    for(i=0;i<opps.length;i++){
      
      if(opps[i].pokemonSet!=undefined )
      oppsInfo.push({id:opps[i].id,
          name:opps[i].name,
          pokemon: this.convertPokemon(opps[i].pokemonSet!)
      });
    }
    return oppsInfo;
  }

  convertPokemon(pokes:Pokemon[]):PokemonInfo[]{

    let i:number;
    let pokesInfo:PokemonInfo[]= new Array();    
    for( i =0;i<pokes.length;i++){
      let p:Pokemon = pokes[i];      
        this.poke.getPokeInfo(p.api_id).subscribe((res)=>{         
          pokesInfo.push({id: p.id,api_id:p.api_id,name:res.name,
            atk:this.poke.calculateStat(res.stats[1].base_stat),
            hp:this.poke.calculateHP(res.stats[0].base_stat),
            def:this.poke.calculateStat(res.stats[2].base_stat),
            move1:this.movePokemon(p.move_1),
            move2:this.movePokemon(p.move_2),
            move3:this.movePokemon(p.move_3),
            move4:this.movePokemon(p.move_4),
            sprite:res.sprites.back_default,
            opp_sprite:res.sprites.front_default            
          });
        });
    }  
    return pokesInfo;   

  }
  movePokemon(move_id:number):Move{
    let move:Move = {id:move_id};
    this.poke.getMoveInfo(move_id).subscribe((res)=>{      
      move.name=res.name;
      move.power=res.power;
      return move;

    })
    return move;
    
  }
  choosePokemon(pokeid:number){
    this.pokeChoice=this.pokesInfo[pokeid];
    

  }
  chooseOpponent(oppid:number){
    let opp = this.oppsInfo[oppid];
    let p =opp.pokemon;
    this.oppChoice = p[0];
this.oppSprite=this.trSprites[oppid];

  }

}
