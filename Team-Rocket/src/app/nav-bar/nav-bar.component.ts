import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterServiceService } from '../register-service.service';
import { User } from '../user-profile/user';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  isLoggedIn: boolean = false;
  user:User;

  constructor(private reg: RegisterServiceService,private router:Router) {

    this.user={id:0,username:"",email:"",avatar:0};
    this.reg.user.subscribe((res)=>{
      console.log("aksdfadkf;j")
        this.user=res;
        if(res){
          sessionStorage.setItem("username",res.username);              
          this.isLoggedIn = true;          
        }else{
        }
      });
      if(sessionStorage.getItem("username")!=""&&sessionStorage.getItem("username")){
        this.isLoggedIn=true;
        this.user.username=sessionStorage.getItem("username")||"";
      }
   }   
  ngOnInit(): void {

  }
  logout(): void {
    this.user = {id:0,username:"",email:"",avatar:0};
    this.reg.user.emit(this.user);
    this.isLoggedIn=!this.isLoggedIn;
    sessionStorage.clear();
    this.router.navigate(["home"]);
    
  }

}
