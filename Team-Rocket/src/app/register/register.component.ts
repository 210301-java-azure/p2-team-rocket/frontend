import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { RegisterServiceService } from '../register-service.service';
import { User } from '../user-profile/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username:string="";
  password:string="";
  email:string="";

  constructor(private reg:RegisterServiceService, private router:Router) {
    
   }

  ngOnInit(): void {
  }

  register(): void{
    this.reg.attemptRegister(this.username, this.password, this.email).subscribe((res)=>{
      let temp: User = {id:res.userId,username:res.username,email:res.email,avatar:res.avatarId};   
      this.reg.user.emit(res);         

      this.router.navigate(["home"]);
    })
    
  }
}
